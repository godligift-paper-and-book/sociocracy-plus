## Карта организации

**Карта организации** - это удобное графическое представление организации в виде "бабл" диаграммы. 

Карта организации включает в себя схематичное изображение доменов: кругов и ролей, типы доменов: открытые или закрытые, делегируемые и вакантные домены (зоны влияния), а также различные связи между доменами (делегирование, представительство и другие).
