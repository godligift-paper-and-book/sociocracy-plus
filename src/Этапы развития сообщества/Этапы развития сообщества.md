## Этапы развития сообщества



1. Тусовка
2. Флэш-группа. Группа разового мероприятия
3. Сообщество с одним устойчивым ядром, клуб с устойчивым ядром
4. Система / Экосистема нескольких устойчивых ядер с общей аурой
   1. Система независимых команд(ядер). (В книге "неформальная социотехнике" названа "кругами", в метаверсите этому состоянию соответсвует понятие "среда")
   2. Система связных команд(ядер). (В книге "неформальная социотехника" cобственно "система", "открытая система" в S3).  
5. Сеть: Сетевая неформальная организация. 





По уровню  группового  сознания  различают следующие виды групп (по Л.И.Уманскому):

Группа-конгломерат -  еще не осознавшая единой цели своей деятельности [31] (аналогичны этому понятия диффузная [23] или номинальная [15] группы).

Группа-ассоциация - имеющая  общую  цель;  все  остальные признаки (подготовленность,  организационное и психологическое единство) отсутствуют [15].

Группа-кооперация - характеризуется единством целей и деятельности, наличием группового опыта и подготовленности [15].

Группа-корпорация -  которую выше кооперации ставит наличие организационного  и психологического единства [15] (иногда такую группу называют автономной [31]). Для корпорации  характерно  проявление  группового эгоизма (противопоставление себя другим группам,  личностям,  обществу) и индивидуализма вплоть до асоциальности [15] (например, банда).









При прочих  равных условиях коллектив обладает наибольшей эффективностью (особенно при решении задач с  ярко  выраженным
общественным содержанием), наиболее человечными межличностными отношениями, большей стойкостью к трудностям.