### Организатор встречи

**Выберите человека, который будет отвечать за подготовку встреч, образовательных и других мероприятий, а также организационные вопросы и последующие действия после их окончания (follow-up).**

Можно брать на себя роль организатора конкретного события или нескольких  событий в течение определенного периода времени.

**Обязанности организатора встречи**

**Подготовка:**
- Определить цели и конечные результаты
- Подготовить повестку и разослать ее участникам
- Определить и пригласить участников
- Оценить, сколько потребуется времени и запланировать встречу/мероприятие
- Забронировать помещение для проведения встречи (и транспорт, если потребуется)
- Подготовить помещение и обеспечить наличие необходимых материалов и информации
- Обеспечить выбор фасилитатора и секретаря для ведения протокола, если необходимо

**После встречи:**
- обеспечить уборку помещения, вернуть ключи, решить все оставшиеся вопросы и обеспечить участников копиями протокола и итоговыми раздаточными материалами.

См. также: Фасилитация встреч, Подготовка к встречам